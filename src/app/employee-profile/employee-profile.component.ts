import {Component, OnInit} from '@angular/core';
import {EmployeeService} from '../services/employee.service';
import {Employee} from '../dto/employee';

@Component({
    selector: 'app-user-profile',
    templateUrl: './employee-profile.component.html',
    styleUrls: ['./employee-profile.component.css']
})
export class EmployeeProfileComponent implements OnInit {

    employee: Array<Employee> = [];
    selectedEmployee: Employee = new Employee();
    // model: any = {
    //     employeeName: '',
    //     designation: '',
    //     address: '',
    //     salary: '',
    //     joinDate: '',
    //     gender: '',
    //     contactNo: '',
    //     dob: ''
    // };

    constructor(private employeeService: EmployeeService) {
    }

    ngOnInit() {
        this.fetchEmployees();
    }

    fetchEmployees() {
        this.employeeService.findAllEmployees().subscribe(result => {
            this.employee = result;
        })
    }

    saveEmployee() {
        this.employeeService.saveEmployee(this.selectedEmployee).subscribe(result => {
            if (result) {
                // swal({
                //     position: 'top-end',
                //     type: 'success',
                //     title: 'Employee has been saved successfully !',
                //     showConfirmButton: false,
                //     timer: 1500
                // });
                console.log('done')
                this.clearForm();
                this.fetchEmployees();
            } else {
                // swal({
                //     position: 'top-end',
                //     type: 'error',
                //     title: 'Oops...',
                //     text: 'Failed to Save the Employee !',
                //     showConfirmButton: false,
                //     timer: 1500
                // });
                console.log('fail')
            }
        })
    }

    clearForm() {
        this.selectedEmployee.name = '';
        this.selectedEmployee.address = '';
        this.selectedEmployee.contactNo = null;
        this.selectedEmployee.designation = '';
        this.selectedEmployee.salary = null;
        this.selectedEmployee.dob = '';
        this.selectedEmployee.gender = '';
        this.selectedEmployee.joinDate = '';
    }

}
