import {Component, OnInit} from '@angular/core';
import {Employee} from '../dto/employee';
import {EmployeeService} from '../services/employee.service';
import {Deduction} from '../dto/deduction';
import {DeductionService} from '../services/deduction.service';

@Component({
    selector: 'app-typography',
    templateUrl: './typography.component.html',
    styleUrls: ['./typography.component.css']
})
export class TypographyComponent implements OnInit {

    selectedDeduction: Deduction = new Deduction();
    employee: Array<Employee> = [];
    selectedEmployee: Employee = new Employee();
    employeeName: string = null;
    employeeId: number;

    constructor(private employeeService: EmployeeService, private deductionService: DeductionService) {
    }

    ngOnInit() {
        this.loadEmployeeId()
    }

    loadEmployeeId(): void {
        this.employeeService.findAllEmployees().subscribe((result) => {
                this.employee = result;
            }
        );
    }

    getEmployees(): void {
        for (let empl of this.employee) {
            if (empl.name == this.employeeName) {

                this.employeeId = empl.employeeId;
                this.selectedEmployee = empl;
            }
        }
    }

    saveDeduction() {
        this.deductionService.saveDeduction(this.selectedDeduction).subscribe(result => {
            if (result) {
                console.log('done')
            } else {
                console.log('fail')
            }
        })
    }

}
