export class Employee {
    employeeId: number;
    name: string;
    designation: string;
    salary: number;
    address: string;
    contactNo: number;
    joinDate: string;
    dob: string;
    gender: string;
}
