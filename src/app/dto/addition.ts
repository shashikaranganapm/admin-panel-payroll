import {Employee} from './employee';
import {Types} from './types';

export class Addition {
    additionId: number;
    additionDate: string;
    totalAmount: number;
    employee_employeeId: Employee;
    types_typeId: string;
}

