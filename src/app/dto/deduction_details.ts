import {Types} from './types';

export class DeductionDetail {
    id:number
    deductionId: number;
    amount: number;
    types_typeId: Types;
}
