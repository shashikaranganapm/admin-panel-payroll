import {Employee} from './employee';

export class Deduction {
    deductionId: number;
    deductionDate: string;
    totalAmount: number;
    employee_employeeId: Employee;
    types_typeId: string;
}
