import { TestBed } from '@angular/core/testing';

import { DeductionService } from './deduction.service';

describe('DeductionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeductionService = TestBed.get(DeductionService);
    expect(service).toBeTruthy();
  });
});
