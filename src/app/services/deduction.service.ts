import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Deduction} from '../dto/deduction';

export const MAIN_URL = 'http://localhost:8080';
const URL = '/api/v1/deduction';
// const endpoint = 'http://localhost:8080/api/v1/employee';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DeductionService {

  constructor(private http: HttpClient) {
  }

  saveDeduction(deduction: Deduction): Observable<boolean> {
    return this.http.post<boolean>(MAIN_URL + URL, deduction, httpOptions).pipe(
        tap((response) => console.log('Deduction added', response)),
        catchError(this.handleError<any>('saveDeduction'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
