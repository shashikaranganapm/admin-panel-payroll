import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Employee} from '../dto/employee';
import {catchError, tap} from 'rxjs/operators';
import {Addition} from '../dto/addition';

export const MAIN_URL = 'http://localhost:8080';
const URL = '/api/v1/addition';
// const endpoint = 'http://localhost:8080/api/v1/employee';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdditionService {

  constructor(private http: HttpClient) {
  }

  saveAddition(addition: Addition): Observable<boolean> {
    return this.http.post<boolean>(MAIN_URL + URL, addition, httpOptions).pipe(
        tap((response) => console.log('Addition added', response)),
        catchError(this.handleError<any>('saveAddition'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
