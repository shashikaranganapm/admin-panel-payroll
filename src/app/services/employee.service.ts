import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Employee} from '../dto/employee';
import {catchError, tap} from 'rxjs/operators';

export const MAIN_URL = 'http://localhost:8080';
const URL = '/api/v1/employee';
// const endpoint = 'http://localhost:8080/api/v1/employee';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {

    constructor(private http: HttpClient) {
    }

    findAllEmployees(): Observable<Array<Employee>> {
        return this.http.get<Array<Employee>>(MAIN_URL + URL);
    }


    saveEmployee(employee: Employee): Observable<boolean> {
        return this.http.post<boolean>(MAIN_URL + URL, employee, httpOptions).pipe(
            tap((response) => console.log('Employee added', response)),
            catchError(this.handleError<any>('saveEmployee'))
        );
    }

    updateEmployee(employee: Employee): Observable<any> {
        return this.http.put<any>(MAIN_URL + URL, employee, httpOptions);
    }

    findEmployee(employeeId: number): Observable<any> {
        return this.http.get<any>(MAIN_URL + URL + '/' + employeeId);
    }

    deleteEmployee(employeeId: number): Observable<any> {
        return this.http.delete<any>(MAIN_URL + URL + '/' + employeeId);
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result..
            return of(result as T);
        };
    }
}
