import {Component, OnInit} from '@angular/core';
import {Addition} from '../dto/addition';
import {Employee} from '../dto/employee';
import {EmployeeService} from '../services/employee.service';
import {AdditionService} from '../services/addition.service';

@Component({
    selector: 'app-table-list',
    templateUrl: './table-list.component.html',
    styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

    additions: Array<Addition> = [];
    selectedAddition: Addition = new Addition();
    employee: Array<Employee> = [];
    selectedEmployee: Employee = new Employee();
    employeeName: string = null;
    employeeId: number;

    constructor(private employeeService: EmployeeService, private additionService: AdditionService) {
    }

    ngOnInit() {
        this.loadEmployeeId()
    }

    loadEmployeeId(): void {
        this.employeeService.findAllEmployees().subscribe((result) => {
                this.employee = result;
            }
        );
    }

    getEmployees(): void {
        for (let empl of this.employee) {
            if (empl.name == this.employeeName) {

                this.employeeId = empl.employeeId;
                this.selectedEmployee = empl;
            }
        }
    }

    saveAddition() {
        this.additionService.saveAddition(this.selectedAddition).subscribe(result => {
            if (result) {
                console.log('done')
            } else {
                console.log('fail')
            }
        })
    }
}


